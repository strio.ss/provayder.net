// ---------mobile-burger------------------------//
document.addEventListener("DOMContentLoaded", ()=>{
    document.querySelector(".burger").addEventListener("click", ()=>{
        document.querySelector("body").classList.toggle("menu-open");
    });
});
/* ----------Секция "Баннер - search-tariffs" ---------------*/
document.addEventListener("DOMContentLoaded", ()=>{
    let intervalId;
    const item = document.querySelector('.form__select-where');
    const body = document.querySelector('.form__select-where__body');
    const header = document.querySelector('.form__select-where__heading');
    // отслеживаем событие "клик" по  элементу '.selection-items__heading'
    if(header){
        header.addEventListener('click', () => {
        // проверяем, если элемент '.selection-items__body' не содержит класс 'selection-open', добавляем классы 'selection-open' ,'selection-active' элементу 'form__select-where__body' и класс 'selection-rotate' элементу 'form__select-where__heading' 
        if(!body.classList.contains('selection-open')){
            body.classList.add('selection-active');
            item.classList.add('selection-rotate');

            // записываем текс выбранного элемента .form__select-where__body__item' и записываем его в 'form__select-where__heading'
            document.querySelectorAll('.form__select-where__body__item').forEach(e =>{
            e.addEventListener('click', e => {
                let text = e.currentTarget.textContent;
                console.log (text);
                header.textContent = text;
                //удаляем классы 'selection-open'  'selection-active' и 'selection-rotate'
                clearInterval(intervalId);
                body.classList.remove('selection-active');
                item.classList.remove('selection-rotate');

                intervalId=setTimeout(()=>{
                    body.classList.remove('selection-open');
                }, 0);
            });
        });

            intervalId=setTimeout(()=>{
                body.classList.add('selection-open');
            }, 0);
        };
        // проверяем, если элемент '.selection-items__body' содержит класс 'selection-open', очищаем интервал и удаляем классы 'selection-open'  'selection-active' и 'selection-rotate'
        if(body.classList.contains('selection-open')){
            clearInterval(intervalId);
            body.classList.remove('selection-active');
            item.classList.remove('selection-rotate');

            intervalId=setTimeout(()=>{
                body.classList.remove('selection-open');
            }, 0);
        };
        // проверям, если событие "клик" произошло  вне зоне нашего открытого элемента, удаляем классы
    });

    }
});
/* ----------Секция "all-tariffs" ---------------*/
document.addEventListener("DOMContentLoaded", ()=>{
    let intervalId;
    // ищем все элементы '.choose-items'
    document.querySelectorAll('.choose-items').forEach(e =>{
        const item = e;
        // ищем все элементы '.choose__heading'
        e.querySelectorAll('.choose__heading').forEach(e =>{
            // отслеживаем событие "клик" по каждому элементу '.choose__headingg'
            e.addEventListener('click', e => {
            // записываем в переменную значение атрибута 'data-head'
            const menu = e.currentTarget.dataset.head;
            const header = e.target;
           
            //  ищем все элементы '.choose__body'
            document.querySelectorAll('.choose__body').forEach(e =>{
                // записываем в переменную элемент '.choose__body' с атрибутом 'data-body', который совпадает с атрибутом 'data-head'
                const submenu = document.querySelector(`[data-body = ${menu}]`);
                // проверяем, если элемент '.choose__body' не содержит класс 'selection-open', удаляем классы 'selection-open' и 'selection-active' - у открытых элементов, затем добавляем классы, чтобы отобразить '.selection-items__body' у текущего элемента
                if(!submenu.classList.contains('selection-open')){
                    e.classList.remove('selection-active');
                    e.classList.remove('selection-open');
                    document.querySelectorAll('.selection-items').forEach(e =>{
                        e.classList.remove('selection-rotate');
                    });

                    submenu.classList.add('selection-active');
                    item.classList.add('selection-rotate');

                    intervalId=setTimeout(()=>{
                        submenu.classList.add('selection-open');
                    }, 0)
                };
                 // проверяем, если элемент '.selection-items__body' содержит класс 'selection-open', очищаем интервал и удаляем классы 'selection-open' и 'selection-active' 
                if(submenu.classList.contains('selection-open')){
                    clearInterval(intervalId);
                    submenu.classList.remove('selection-active');
                    item.classList.remove('selection-rotate');

                    intervalId=setTimeout(()=>{
                        submenu.classList.remove('selection-open');
                    }, 0)

                };
            });
        });
    });
    });
});
document.addEventListener("DOMContentLoaded", ()=>{
    document.querySelectorAll(".choose__body__item").forEach((el)=>{
        el.addEventListener("click", (e)=>{
           const el_input = el.querySelector("input");
           if(el_input){
               let atr_checked = el_input.getAttribute("checked");
               if (atr_checked == 'checked'){
                   el_input.removeAttribute("checked");

               } else {
                el_input.setAttribute("checked", "checked");
               }
           }
        });
    });
})
        /*----"questions-provider"------------------  */
document.addEventListener("DOMContentLoaded", ()=>{
    document.querySelectorAll(".questions-provider-wrap__item").forEach((item) =>{
        item.addEventListener('click', ()=>{
            let child = item.querySelector('.questions-provider-wrap__item-body')

            if (item.classList.contains('active')){
                item.classList.remove('active')
                child.style.maxHeight = null

            } else {
                [].forEach.call(item.parentNode.querySelectorAll('.questions-provider-wrap__item.active'), (el) => {
                    el.classList.remove('active')
                } );

                item.classList.add('active');

                document.querySelectorAll('.questions-provider-wrap__item-body').forEach((el)=>{
                    el.style.maxHeight = null;
                });

                child.style.maxHeight = child.scrollHeight + 'px';                
            }
        });
    })
});
        // ----------open popup-search-tariffs---------------//
document.addEventListener("DOMContentLoaded", ()=>{
    const btn = document.querySelector(".search-tariffs-btn");
    const request_btn = document.querySelector(".send-request-btn");

    if(btn){
        btn.addEventListener('click', (e)=>{
            document.querySelector('.popup-search-providers').classList.remove('hidden');

                document.querySelectorAll('.popup-search-providers__item svg').forEach((e) =>{
                    e.addEventListener('click', ()=>{
                    document.querySelector('.popup-search-providers').classList.add('hidden');
                })
            });
        });
    }
    // ----------open popup-send-request---------------//
    if(request_btn){
        request_btn.addEventListener('click', (e)=>{
            document.querySelector('.popup-send-request').classList.remove('hidden');

            document.querySelectorAll('.popup-send-request__item svg').forEach((e) =>{
                e.addEventListener('click', ()=>{
                document.querySelector('.popup-send-request').classList.add('hidden');
                })
            });
        });
    }
    // ----------open popup-tariffs-month---------------//
    document.querySelectorAll(".tariffs-month-cards-bottom-btn").forEach((e) => {
        e.addEventListener('click', (e) => {
            document.querySelector('.popup-tariffs-month').classList.remove('hidden');

            document.querySelectorAll('.popup-tariffs-month__item svg').forEach((e) =>{
                e.addEventListener('click', ()=>{
                document.querySelector('.popup-tariffs-month').classList.add('hidden');
                })
            });
        })
    })
    
});
// ----------- open calculator------------//
document.addEventListener("DOMContentLoaded", ()=>{
    document.querySelectorAll(".calculator-click").forEach((e) =>{
        e.addEventListener('click', ()=>{
        // document.querySelector('body').classList.add('no-scroll');
            document.querySelector('.popup-wrap').classList.remove('hidden');
            document.querySelector('.calculator-container').style.display = 'block';

            document.querySelector('.calculator__btn').addEventListener('click', ()=>{
                document.querySelector('.popup-wrap').classList.add('hidden');
                document.querySelector('.calculator-container').style.display = 'none';
            });
        });
    });
});
